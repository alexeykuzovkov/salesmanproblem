//
//  Tour.swift
//  SalesmanProblem
//
//  Created by Alex Kuzovkov on 14.11.2017.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import Foundation

class Tour {
    private var tour = [City?]()
    private var fitness:Double = 0
    private var distance:Int = 0
    
    init() {
        tour = [City?]()
        for _ in 0..<TourManager.numberOfCities() {
            tour.append(nil)
        }
    }
    
    init(tour:[City]) {
        self.tour = tour
    }
    
    func generateRandomTour() {
        self.tour = [City]()
        for i in 0..<TourManager.numberOfCities() {
            self.setCity(tourPosition:i, city:TourManager.getCity(index: i))
        }
        
        tour.shuffle()
    }
    
    func setCity(tourPosition:Int, city:City) {
        if ((tourPosition+1)>tour.count) {
            tour.append(city)
        }
        else {
            tour[tourPosition] = city
        }
        fitness = 0;
        distance = 0;
    }
    func getCity(tourPosition:Int) -> City? {
        return tour[tourPosition];
    }
    
    func getFitness() -> Double{
        if (fitness == 0) {
            fitness = 1/Double(getDistance())
        }
        return fitness;
    }
    
    func getDistance() -> Int {
        if (distance == 0) {
            var tourDistance:Int = 0;
            for cityIndex in 0..<tour.count {
                let fromCity = getCity(tourPosition:cityIndex)!
                let destinationCity = (cityIndex+1 < tour.count) ? getCity(tourPosition:cityIndex+1)!:getCity(tourPosition:0)!
                
                tourDistance += Int(fromCity.distanceTo(city:destinationCity))
            }
            distance = tourDistance;
        }
        return distance;
    }
    
    func contains(city:City) -> Bool{
        let cont = tour.contains {$0 === city}
        return cont
    }
    
    func tourSize() -> Int{
        return tour.count
    }
    
    var description: String {
        var retString = "";
        
        for city in tour {
            retString.append("\(city!.description); ")
        }
        return retString
    }
}

extension Array {
    /**
     Randomizes the order of an array's elements
     */
    mutating func shuffle() {
        for _ in 0..<count {
            sort { (_,_) in arc4random() < arc4random() }
        }
    }
}
