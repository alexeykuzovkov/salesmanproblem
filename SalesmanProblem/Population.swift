//
//  Population.swift
//  SalesmanProblem
//
//  Created by Alex Kuzovkov on 14.11.2017.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import Foundation

class Population {
    var tours = [Tour]()
    
    init(populationSize:Int) {
        tours = [Tour]()
        
        for _ in 0..<populationSize {
            let newTour = Tour();
            newTour.generateRandomTour();
            tours.append(newTour);
        }
        
    }
    
    func saveTour(index:Int, tour:Tour) {
        tours[index] = tour;
    }
    
    func getTour(index:Int) -> Tour {
        return tours[index];
    }
    
    //Получение лучшего из популяции
    func getFittest() ->Tour {
        var fittest = tours[0]
        let popSize = populationSize()
        for i in 1..<popSize {
            if (fittest.getFitness() <= getTour(index:i).getFitness()) {
                fittest = getTour(index:i);
            }
        }
        return fittest;
    }
    
    func populationSize()->Int {
        return tours.count;
    }
}
