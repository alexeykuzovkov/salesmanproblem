//
//  ResultsTableViewController.swift
//  SalesmanProblem
//
//  Created by Alex Kuzovkov on 16.11.2017.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import UIKit
import RealmSwift

protocol ResultsTableViewControllerDelegate {
    func didSelectResult(tableViewController: ResultsTableViewController, result:ResultRLM)
}

class ResultsTableViewController: UITableViewController {

    let realm = try! Realm()
    var results:Results<ResultRLM>?
    let dateFormatter = DateFormatter()
    var delegate: ResultsTableViewControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        results = realm.objects(ResultRLM.self).sorted(byKeyPath:"date", ascending: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results!.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let dateString = dateFormatter.string(from:results![indexPath.row].date)
        
        cell.textLabel?.text = dateString

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        delegate?.didSelectResult(tableViewController: self, result: results![indexPath.row])
        
        self.dismiss(animated: true, completion: nil)
    }



}
