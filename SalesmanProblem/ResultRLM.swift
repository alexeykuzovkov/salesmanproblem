//
//  ResultRLM.swift
//  SalesmanProblem
//
//  Created by Alex Kuzovkov on 16.11.2017.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import Foundation
import RealmSwift

class ResultRLM: Object {
    @objc dynamic var date = Date()
    let cities = List<CityRLM>()
    let result = List<CityRLM>()
}
