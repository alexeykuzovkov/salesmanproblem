//
//  CityRLM.swift
//  SalesmanProblem
//
//  Created by Alex Kuzovkov on 16.11.2017.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import Foundation
import RealmSwift

class CityRLM: Object {
    @objc dynamic var x = 0
    @objc dynamic var y = 0
    @objc dynamic var index = 0
}
