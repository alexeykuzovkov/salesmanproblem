//
//  TourManager.swift
//  SalesmanProblem
//
//  Created by Alex Kuzovkov on 14.11.2017.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import Foundation

class TourManager {
    static var destinationCities = [City]()
    
    static func addCity(city:City) {
        city.index = numberOfCities()
        destinationCities.append(city);
    }
    
    static func getCity(index:Int) -> City{
        return destinationCities[index];
    }
    
    static func numberOfCities() -> Int{
        return destinationCities.count;
    }
    
    static func removeAllCities() {
        destinationCities = [City]()
    }
}
