//
//  FirstViewController.swift
//  SalesmanProblem
//
//  Created by Alex Kuzovkov on 13.11.2017.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import UIKit

import RealmSwift

class FirstViewController: UIViewController, ResultsTableViewControllerDelegate {
    @IBOutlet var numberOfCitiesLabel: UILabel!
    @IBOutlet var numberOfCitiesSlider: UISlider!
    @IBOutlet var matrixView: MatrixView!
    @IBOutlet var pathLabel: UILabel!
    @IBOutlet var drawMethodSegmentedControl: UISegmentedControl!
    
    @IBOutlet var mapView: PathDrawerView!
    @IBOutlet var solvingProgressView: UIProgressView!
    @IBOutlet var solvingIndicator: UIActivityIndicatorView!
    @IBOutlet var solveButton: UIButton!
    @IBOutlet var resultsButton: UIButton!
    
    var numberOfCities = 3
    var matrix = [Array<Int>]()
  
    //MARK: view lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fillMatrix()
        self.drawMatrix()
        self.clearMap()
        self.updateNumberOfCitiesLabel()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:fill matrix
    
    func fillMatrix() {
        pathLabel.text = "";
        
        TourManager.removeAllCities()
        for _ in 0..<numberOfCities {
            let city = City();
            TourManager.addCity(city:city);
        }
        
        fillMatrixFromTourManager()
    }
    
    func fillMatrixFromLoaded(result:ResultRLM) {
        TourManager.removeAllCities()
        for (_, cityRlm) in result.cities.enumerated() {
            let city = City(x: cityRlm.x, y: cityRlm.y);
            city.index = cityRlm.index
            TourManager.addCity(city:city);
        }
        fillMatrixFromTourManager()
        
        self.drawMatrix()
        
        for i in 0..<result.result.count-1 {
            let sourceCity = result.result[i]
            let destCity = result.result[i+1]
            self.matrixView.matrixColorFill.append((sourceCity.index, destCity.index))
        }
    }
    
    func fillMatrixFromTourManager() {
        matrix = [Array<Int>]()
        for i in 0..<TourManager.numberOfCities() {
            var row = [Int]()
            for j in 0..<TourManager.numberOfCities() {
                if (i==j) {
                    row.append(Int.max)
                }
                else {
                    let sourceCity = TourManager.getCity(index: i);
                    let destCity = TourManager.getCity(index: j);
                    row.append(Int(sourceCity.distanceTo(city: destCity)))
                }
            }
            matrix.append(row)
        }
    }
    
    
    //MARK:draw interface
    
    func updateNumberOfCitiesLabel() {
        numberOfCitiesLabel.text = "\(numberOfCities)"
    }
    
    
    func drawMatrix() {
        matrixView.matrix = matrix
        matrixView.matrixColorFill.removeAll()
    }
    
    func clearMap() {
        mapView.clearPath()
    }
    
    func updateMapViewFromLoaded(result:ResultRLM) {
        self.clearMap()
        for (_, cityRlm) in result.result.enumerated() {
            self.mapView.addToPath(x: cityRlm.x, y: cityRlm.y);
        }
    }
    
    func updatePathLabelFromLoaded(result:ResultRLM) {
        var pathText = ""
        for (i, cityRlm) in result.result.enumerated() {
            pathText+="\(cityRlm.index+1)"
            if (i+1 < result.result.count) {
                pathText+=" -> "
            }
        }
        self.pathLabel.text = pathText
    }
    
    //MARK: IBActions
    @IBAction func didChangeDrawMethod(_ sender: Any) {
        if(drawMethodSegmentedControl.selectedSegmentIndex==0) {
            matrixView.isHidden = false;
            mapView.isHidden = true;
        }
        else {
            matrixView.isHidden = true;
            mapView.isHidden = false;
        }
    }
    @IBAction func didChangeNumberOfCities(_ sender: Any) {
        let newNumberOfCities = Int(numberOfCitiesSlider.value*10)
        
        if (newNumberOfCities != numberOfCities) {
            numberOfCities = newNumberOfCities
            self.fillMatrix()
            self.drawMatrix()
            self.clearMap()
            self.updateNumberOfCitiesLabel()
        }
    }
    
    @IBAction func solvePath(_ sender: Any) {
        self.solvePathInBackground();
    }
    
    
    //MARK: solve
    func solvePathInBackground() {
        self.solveButton.isHidden = true
        self.solvingIndicator.startAnimating()
        self.numberOfCitiesSlider.isHidden = true
        self.solvingProgressView.isHidden = false
        self.solvingProgressView.setProgress(0, animated: false)
        self.pathLabel.text = "";
        self.resultsButton.isHidden = true
        
        self.mapView.clearPath()
        
        DispatchQueue.global(qos: .userInitiated).async {
            var pop = Population(populationSize: 200);
            
            pop = GA.evolvePopulation(pop:pop);
            for i in 0..<100 {
                DispatchQueue.main.async {
                    self.solvingProgressView.setProgress((Float(i)+1)/100, animated: true);
                }
                
                pop = GA.evolvePopulation(pop:pop);
            }
            
            DispatchQueue.main.async {
                var pathText = ""
                
                self.solveButton.isHidden = false;
                self.solvingIndicator.stopAnimating()
                self.numberOfCitiesSlider.isHidden = false;
                self.solvingProgressView.isHidden = true;
                self.resultsButton.isHidden = false
                
                self.matrixView.matrixColorFill.removeAll()
                
                let fittestTour = pop.getFittest()
                for i in 0..<TourManager.numberOfCities() {
                    let sourceCity = fittestTour.getCity(tourPosition: i)!;
                    
                    pathText+="\(sourceCity.index+1)"
                    
                    self.mapView.addToPath(x: sourceCity.x, y: sourceCity.y);
                    
                    if (i+1 < TourManager.numberOfCities()) {
                        pathText+=" -> "
                        
                        let destCity = fittestTour.getCity(tourPosition: i+1)!;
                        self.matrixView.matrixColorFill.append((sourceCity.index, destCity.index))
                    }
                }
                
                self.saveResultToRealm(tour:fittestTour)
                
                self.pathLabel.text = pathText
            }
        }
    }
    
    //MARK: Result Save and load
    
    
    func saveResultToRealm(tour:Tour) {
        let newResult = ResultRLM()
        newResult.date = Date()
        
        for i in 0..<TourManager.numberOfCities() {
            let initialOrderCity = TourManager.getCity(index: i)
            
            let initialCityRLM = CityRLM()
            initialCityRLM.x = initialOrderCity.x
            initialCityRLM.y = initialOrderCity.y
            initialCityRLM.index = initialOrderCity.index
        
            newResult.cities.append(initialCityRLM)
            
            let tourCity = tour.getCity(tourPosition: i)!
            
            let tourCityRLM = CityRLM()
            tourCityRLM.x = tourCity.x
            tourCityRLM.y = tourCity.y
            tourCityRLM.index = tourCity.index
            
            newResult.result.append(tourCityRLM)
        }
        
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(newResult)
        }
    }
    
    func didSelectResult(tableViewController: ResultsTableViewController, result: ResultRLM) {
        fillMatrixFromLoaded(result:result);
        
        numberOfCitiesSlider.value = Float(result.cities.count)*0.1
        
        numberOfCities = result.cities.count
        updateNumberOfCitiesLabel()
        
        updateMapViewFromLoaded(result:result)
        updatePathLabelFromLoaded(result:result)
    }
    
    
    //MARK: navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier=="Results") {
            let nav = segue.destination as! UINavigationController
            let resultsTVC = nav.viewControllers[0] as! ResultsTableViewController
            resultsTVC.delegate = self
        }
    }
}

