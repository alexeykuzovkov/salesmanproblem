//
//  City.swift
//  SalesmanProblem
//
//  Created by Alex Kuzovkov on 14.11.2017.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import Foundation

class City {
    var x = 0
    var y = 0
    var index = 0
    
    init() {
        x = Int(arc4random_uniform(200))
        y = Int(arc4random_uniform(200))
    }
    
    init(x:Int, y:Int) {
        self.x = x
        self.y = y
    }

    
    func distanceTo(city:City) -> Double {
        
        let xDistance = abs(self.x - city.x);
        let yDistance = abs(self.y - city.y);
        
        let distance = sqrt( Double((xDistance*xDistance) + (yDistance*yDistance)) );
    
        return distance;
    }
    
    var description: String {
        return "index:\(index), x:\(self.x), y:\(self.y)"
    }
}
