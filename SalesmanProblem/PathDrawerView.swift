//
//  PathDrawerView.swift
//  SalesmanProblem
//
//  Created by Alex Kuzovkov on 15.11.2017.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import UIKit

@IBDesignable
class PathDrawerView: UIView {

    var path:[(x:Int, y:Int)] = [(x:10, y:10), (x:150, y:200), (x:250, y:10)] {didSet {setNeedsDisplay()}}
    
    private var points = [CAShapeLayer]()
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    func addToPath(x:Int, y:Int) {
        path.append((x:x, y:y))
    }
    
    func clearPath() {
        path.removeAll()
    }
    
    func drawPath() {
        if (path.count==0) {
            return
        }
        
        let aPath = UIBezierPath()
        
        aPath.move(to:CGPoint(x:path[0].x, y:path[0].y))
        
        for point in path {
            aPath.addLine(to:CGPoint(x:point.x, y:point.y))
        }
        
        aPath.close()
        UIColor.red.set()
        aPath.stroke()
    }
    
    func drawPoints() {
        for l in points {
            l.removeFromSuperlayer()
        }
        points.removeAll()
        
        for point in path {
            let circlePath = UIBezierPath(arcCenter: CGPoint(x: point.x,y: point.y), radius: CGFloat(10), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
    
            let shapeLayer = CAShapeLayer()
            shapeLayer.path = circlePath.cgPath
            shapeLayer.fillColor = UIColor.clear.cgColor
            shapeLayer.strokeColor = UIColor.red.cgColor
            shapeLayer.lineWidth = 3.0
            
            points.append(shapeLayer)
            
            self.layer.addSublayer(shapeLayer)
        }
    }
    
    override func draw(_ rect: CGRect) {
        drawPath()
        drawPoints()
    }

}
