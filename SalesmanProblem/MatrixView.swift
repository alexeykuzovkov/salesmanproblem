//
//  MatrixView.swift
//  SalesmanProblem
//
//  Created by Alex Kuzovkov on 13.11.2017.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import UIKit

@IBDesignable

class MatrixView: UIView {
    @IBInspectable var matrix:    [Array<Int>] =  [[Int.max, 10, 11, 12],[10,Int.max,11, 12],[10,11,Int.max, 12],[10,11, 12,Int.max]] {didSet { updateView() }}
    @IBInspectable var showIndex:  Bool =  false {didSet { updateView() }}
    
    var matrixColorFill: [(Int, Int)] = [(0, 1), (1, 2), (2, 3)] {didSet { updateView() }}
    
    func updateView() {
        for view in self.subviews {
            view.removeFromSuperview();
        }
        
        let indexShift = (showIndex) ? 1 : 0;
        
        let labelSize = Int(Int(self.frame.size.width)/((matrix.count)+indexShift))
        
        if (showIndex) {
            for (i,_) in matrix.enumerated() {
                let colLabel = UILabel.init(frame: CGRect(x: (i+1)*labelSize, y: 0, width: labelSize, height: labelSize))
                colLabel.textAlignment = .center
                colLabel.font = UIFont.systemFont(ofSize: 12)
                colLabel.text = "\(i+1)"
                self.addSubview(colLabel)
            
                let rowLabel = UILabel.init(frame: CGRect(x: 0, y: (i+1)*labelSize, width: labelSize, height: labelSize))
                rowLabel.textAlignment = .center
                rowLabel.font = UIFont.systemFont(ofSize: 12)
                rowLabel.text = "\(i+1)"
                self.addSubview(rowLabel)
            }
        }
    
        for (i,row) in matrix.enumerated() {
            for (j,col) in row.enumerated() {
                let cellLabel = UILabel.init(frame: CGRect(x: (i+indexShift)*labelSize, y: (j+indexShift)*labelSize, width: labelSize, height: labelSize))
                cellLabel.textAlignment = .center
                cellLabel.font = UIFont.systemFont(ofSize: 12)
                
                if let index = matrixColorFill.index(where:{ $0 == (j,i) }) {
                    let alphaIndex = Float(Float(index+1)/Float(matrixColorFill.count))
                    cellLabel.backgroundColor = UIColor.init(colorLiteralRed: 1, green: 0, blue: 0, alpha: alphaIndex)
                }
                
                if (col<Int.max) {
                    cellLabel.text = "\(col)"
                }
                else {
                    cellLabel.text = "X"
                }
                
                self.addSubview(cellLabel)
            }
        }
    }
    
    func contains(a:[(Int, Int)], v:(Int,Int)) -> Bool {
        let (c1, c2) = v
        for (v1, v2) in a { if v1 == c1 && v2 == c2 { return true } }
        return false
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateView()
    }

}
