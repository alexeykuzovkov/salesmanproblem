//
//  GA.swift
//  SalesmanProblem
//
//  Created by Alex Kuzovkov on 14.11.2017.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

import Foundation
class GA {
    private static var mutationRate:Double = 0.015;
    private static var tournamentSize:Int = 5;
    private static var elitism:Bool = true;
    
    // Развить популяцию на одно поколение
    public static func evolvePopulation(pop:Population) -> Population {
        let newPopulation = Population(populationSize:pop.populationSize());
    
        //Elitism: сохранить лучшего из предыдущего поколения в новом
        var elitismOffset:Int = 0;
        if (elitism) {
            newPopulation.saveTour(index:0, tour:pop.getFittest());
            elitismOffset = 1;
        }
    
        // Скрещивание в популяции
        for i in elitismOffset..<newPopulation.populationSize() {
            let parent1:Tour = tournamentSelection(pop:pop);
            let parent2:Tour = tournamentSelection(pop:pop);
            
            let child:Tour = crossover(parent1:parent1, parent2:parent2);
            
            newPopulation.saveTour(index:i, tour:child);
        }
    
        // Рандомные мутации в новой популяции
        for i in elitismOffset..<newPopulation.populationSize() {
            mutate(tour:newPopulation.getTour(index:i));
        }
    
        return newPopulation;
    }
    
    // Скрещивание "родителей" для получения потомка
    public static func crossover(parent1:Tour, parent2:Tour) ->Tour {
        let child = Tour();
    
        // Получение рандомного промежутка в маршруте родителя 1
        let startPos:Int = Int(arc4random_uniform(UInt32(parent1.tourSize())));
        let endPos:Int = Int(arc4random_uniform(UInt32(parent1.tourSize())));
    
        // Добавление городов из рандомного промежутка из маршрута 1 потомку на те же места
        for i in 0..<child.tourSize() {
            
            if (startPos < endPos && i > startPos && i < endPos) {
                child.setCity(tourPosition:i, city:parent1.getCity(tourPosition:i)!);
            }
            else if (startPos > endPos) {
                if (!(i < startPos && i > endPos)) {
                    child.setCity(tourPosition:i, city:parent1.getCity(tourPosition:i)!);
                }
            }
        }
    
        // Добавление потомку городов родителя 2, которых у потомка еще нет на свободные места, сохраняя порядок следования
        for i in 0..<parent2.tourSize() {
            if (!child.contains(city:parent2.getCity(tourPosition:i)!)) {
                for ii in 0..<child.tourSize() {
                    if (child.getCity(tourPosition:ii) == nil) {
                        child.setCity(tourPosition:ii, city:parent2.getCity(tourPosition:i)!);
                        break;
                    }
                }
            }
        }
        return child;
    }
    
    //Мутация: смена мест
    private static func mutate(tour:Tour) {
        for tourPos1 in 0..<tour.tourSize() {
            // Проверка вероятности мутации
            if(Double(arc4random()) < mutationRate){
                // Получение второго рандомного города
                let tourPos2 = Int(UInt32(tour.tourSize()) * arc4random());
    
                //Смена мест
                let city1:City = tour.getCity(tourPosition:tourPos1)!;
                let city2:City = tour.getCity(tourPosition:tourPos2)!;
                
                tour.setCity(tourPosition:tourPos2, city:city1);
                tour.setCity(tourPosition:tourPos1, city:city2);
            }
        }
    }
    
    // Выбор кандидатов для формирования потомства
    private static func tournamentSelection(pop:Population) -> Tour {
        
        //Создание популяции, создающей потомство и заполнение рандомными маршрутами
        let tournament:Population = Population(populationSize:tournamentSize);
        for i in 0..<tournamentSize {
            let randomId = Int(arc4random_uniform(UInt32(pop.populationSize())));
            tournament.saveTour(index:i, tour:pop.getTour(index:randomId));
        }
        
        // Выбор лучшего
        let fittest = tournament.getFittest();
        return fittest;
    }
}
